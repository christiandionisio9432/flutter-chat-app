import 'dart:convert';

import 'package:chat_app/global/environments.dart';
import 'package:chat_app/models/login_response.dart';
import 'package:chat_app/models/usuario.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class AuthService with ChangeNotifier {
  late Usuario usuario;
  bool _autenticando = false;

  // Create storage
  final _storage = new FlutterSecureStorage();

  bool get autenticando => this._autenticando;
  set autenticando(bool valor) {
    this._autenticando = valor;
    notifyListeners();
  }

  // Getters del token de forma estatica
  static Future<String> getToken() async {
    final _storage = new FlutterSecureStorage();
    final token = await _storage.read(key: 'token');
    return token!;
  }

  static Future<void> deleteToken() async {
    final _storage = new FlutterSecureStorage();
    final token = await _storage.delete(key: 'token');
  }

  Future<bool> login(String email, String password) async {
    this.autenticando = true;

    final data = {'email': email, 'password': password};

    final resp = await http.post(
      Uri.parse('${Environent.apiUrl}/login'),
      body: jsonEncode(data),
      headers: {'Content-Type': 'application/json'},
    );

    if (resp.statusCode == 200) {
      final loginResponse = LoginResponse.fromJson(resp.body);
      this.usuario = loginResponse.usuario;
      this.autenticando = false;

      await this._guardarToken(loginResponse.token);

      return true;
    } else {
      this.autenticando = false;
      return false;
    }
  }

  Future register(String nombre, String email, String password) async {
    this.autenticando = true;

    final data = {'nombre': nombre, 'email': email, 'password': password};

    final resp = await http.post(
      Uri.parse('${Environent.apiUrl}/login/new'),
      body: jsonEncode(data),
      headers: {'Content-Type': 'application/json'},
    );

    if (resp.statusCode == 200) {
      final loginResponse = LoginResponse.fromJson(resp.body);
      this.usuario = loginResponse.usuario;
      this.autenticando = false;

      await this._guardarToken(loginResponse.token);

      return true;
    } else {
      this.autenticando = false;
      final Map<String, dynamic> respBody = jsonDecode(resp.body);
      String errores = _getErrorMessages(respBody);
      return errores;
    }
  }

  Future<bool> isLoggedIn() async {
    final token = await this._storage.read(key: 'token');

    final resp = await http.get(
      Uri.parse('${Environent.apiUrl}/login/renew'),
      headers: {
        'Content-Type': 'application/json',
        'x-token': token ?? '',
      },
    );

    if (resp.statusCode == 200) {
      final loginResponse = LoginResponse.fromJson(resp.body);
      this.usuario = loginResponse.usuario;

      await this._guardarToken(loginResponse.token);

      return true;
    } else {
      this.logout();
      return false;
    }
  }

  Future _guardarToken(String token) async {
    return await _storage.write(key: 'token', value: token);
  }

  Future logout() async {
    await _storage.delete(key: 'token');
  }

  String _getErrorMessages(Map<String, dynamic> respBody) {
    String errorsMsg = '';
    Map<String, dynamic> errores = respBody['errors'] ?? {};

    if (!errores.isEmpty) {
      errores.forEach((key, value) {
        Map<String, dynamic> mensajes = value;
        errorsMsg += mensajes['msg'] + '\n';
      });

      return errorsMsg;
    }

    return respBody['msg'];
  }
}
