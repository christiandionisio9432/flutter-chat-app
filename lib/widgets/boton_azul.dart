import 'package:flutter/material.dart';

class BotonAzul extends StatelessWidget {
  final String text;
  // final void Function() eventOnPress;
  final VoidCallback? onPressed;

  const BotonAzul({
    Key? key,
    required this.text,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      elevation: 2,
      shape: StadiumBorder(),
      color: Colors.blue,
      highlightElevation: 5,
      disabledColor: Colors.grey,
      onPressed: this.onPressed,
      child: Container(
        width: double.infinity,
        height: 55,
        child: Center(
          child: Text(this.text,
              style: TextStyle(color: Colors.white, fontSize: 17)),
        ),
      ),
    );
  }
}
