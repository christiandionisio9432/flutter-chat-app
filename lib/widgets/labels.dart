import 'package:flutter/material.dart';

class Labels extends StatelessWidget {
  final String ruta;

  const Labels({Key? key, required this.ruta}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Text(
            (ruta != 'login') ? '¿No tienes cuenta?' : '¿Ya tienes una cuenta?',
            style: TextStyle(
              color: Colors.black54,
              fontSize: 15,
              fontWeight: FontWeight.w300,
            ),
          ),
          SizedBox(height: 10),
          GestureDetector(
            child: Text(
              (ruta != 'login') ? 'Crea una ahora!' : 'Inicia sesión aquí!',
              style: TextStyle(
                color: Colors.blue[600],
                fontWeight: FontWeight.bold,
              ),
            ),
            onTap: () {
              Navigator.pushReplacementNamed(context, this.ruta);
            },
          )
        ],
      ),
    );
  }
}
